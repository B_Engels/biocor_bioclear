#!/usr/bin/env python3
"""
This script gives as an output a text file with the top X BLAST results with at least a certain
percentage of identity and with at least a certain alignment length.

    Modules:
        - proces_file returns a list with only the lines that have a minimum alignment length
          and a minimum procent of identity

    Usage:
        python3 process_blast_output.py -f [file] -l [length] -i [identity] -o [output]

    Arguments:
        file        BLAST output file that needs to be processed
        length      Alignment length threshold
        identity    Prercentage of identity threshold
        output      Output file name

    output:
        a text file with BLAST output
"""
import argparse

__author__ = 'Jelle B'
__datum__ = 14-1-2020
__version__ = '2020.v1'


def proces_file(file, length, identity):
    """
    This function loops through the BLAST output and checks per line if the alignment length a certain value
    or higher is and if the percentage of identitiy a certain value or higher is.
    :param file: BLAST ouput file name that needs to be processed
    :param length: Minimum alignment length
    :param identity: Minimum percentage of identity
    :return: a list with the lines that are needed
    """
    result_list = []
    # loops through each line
    for line in file:
        # checks if line not comment
        if not line.startswith("#"):
            split_line = line.split()
            # Checks if line meets the requirements of alignment lenght and percentage of identity
            if int(split_line[3]) > length and float(split_line[2]) > identity and float(split_line[6]) < 0.005:
                result_list.append(line)
    return result_list


def main():
    """
    Main function
    :return:
    """
    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "-file", help="BLAST output file that needs to be processed")
    parser.add_argument('-l', '-length', help="Alignment length threshold.")
    parser.add_argument('-i', '-identity', help="Percentage of identity threshold.")
    parser.add_argument('-o', '-output', help='Output file')

    args = parser.parse_args()
    file = args.f
    length = int(args.l)
    identity = float(args.i)
    output = args.o

    # reads blast file
    file_obj = open(file, 'r')
    results = proces_file(file_obj, length, identity)
    results.sort(key=lambda x: x.split("\t")[2])
    out_file = output

    # writes to output file
    out_data = ''
    for line in results[0:50]:
        out_data += line

    open(out_file, 'w').write(out_data)


if __name__ == '__main__':
    main()
