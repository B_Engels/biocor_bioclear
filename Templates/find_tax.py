#!/usr/bin/env python3
import argparse
import re

def search_for_tax(file, taxonomy_file):
    tax_list = []
    unique_list = []
    with open(taxonomy_file, 'r') as tax_file:
        subject_data = tax_file.readlines()
    with open(file, 'r') as query_file:
        data = query_file.readlines()
        for line in data:
            expression = line.split('\t')[1]
            for tax in subject_data:
                match = re.search(expression, tax)
                if match is not None:
                    if 'organisms;Bacteria' in match.string and 'uncultured bacterium' not in match.string:
                        if match.string.split(';')[-1] not in unique_list:
                            unique_list.append(match.string.split(';')[-1])
                            tax_list.append(match.string)
    return tax_list


def report_taxonomy(tax_list):
    unique_organisms = len(tax_list)
    unique_count_string = "There are {} unique bacteria in this taxonomy list.".format(unique_organisms)
    sulf_redux_count = 0
    for organism in tax_list:
        match = re.search("Desulf", organism)
        if match:
            sulf_redux_count += 1
    sulf_string_count = "There are {} Sulfur/Sulfate reducing bacteria in this taxonomy list.".format(sulf_redux_count)
    messages = [unique_count_string, sulf_string_count]
    return messages


def write_report(tax_list, count_report, out_path):
    out_file = out_path+"taxonomy_report.txt"
    organisms = ''
    for line in tax_list:
        organisms += line
    with open(out_file, 'w') as out:
        out.write(count_report[0]+'\n')
        out.write(count_report[1]+2*'\n')
        out.write(organisms)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '-taxfile', help="Path of the taxonomy text file that you would like to use.")
    parser.add_argument('-f', '-blast_output_file', help="Path of the blast output text file that you wouldto process.")
    parser.add_argument('-o', '-output_path', help="Path of the folder where the taxonomy report will be placed.")

    args = parser.parse_args()
    file = args.f
    tax_file = args.t
    out_path = args.o
    tax = search_for_tax(file, tax_file)
    messages = report_taxonomy(tax)
    write_report(tax, messages, out_path)


if __name__ == '__main__':
    main()
