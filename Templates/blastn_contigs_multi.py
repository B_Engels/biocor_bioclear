#!/usr/bin/env/ python3

"""
this file makes it possible to perform a blastn with multiple fasta files.
the arguments are coded in the this program. these are: a self made silva database.
and the output file contains of:
7 qaccver sallacc sacverr pident length mismatch gapopen evalue bitscore score.
next there are these arguments
-subject_besthit -num_alignments '5' -num_threads '30
"""

__author__ = 'Bart Engels'
__data__ = '14-01-2020'
__version__ = "v1_2020"

import sys
import os
import glob
import argparse
from multiprocessing import Pool


class BlastObj:
    """
    This class uses a blastn Query and uses is in a multi process so it is easier
    to do a blastn with multiple files

    """
    def __init__(self, file):
        self.file = file
        self.out = file[0:-6] + "_blast_result.txt"
        self.statement = "blastn -query {} -task 'megablast' -db " \
                         "'/students/2019-2020/Thema06/project-data/bio_" \
                         "corrosie/biocor_bioclear/SILVA_Database/SILVA_DB' -out {} " \
                         "-outfmt '7 qaccver sallacc sacverr pident " \
                         "length mismatch gapopen evalue bitscore score' " \
                         "-subject_besthit -num_alignments '5' " \
                         "-num_threads '30'".format(self.file, self.out)

    def run(self):
        """
        This methode runs the stament
        that is hardcoded in the init

        """
        statement_line = self.statement
        os.system(statement_line)


def run_blast(b_obj):
    """
    Is used in the pool function in the main()
    """
    b_obj.run()


def main(args):
    """
    this makes sure te program can be used.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "-patway", help="patway to directory with files.")
    args = parser.parse_args()
    pathway = args.p
    list_fasta_file = glob.glob(str(pathway) + "*.fasta")
    my_b_objs = list()
    for file in list_fasta_file:
        my_b_objs.append(BlastObj(file))
    nb_processes = len(my_b_objs)

    with Pool(processes=nb_processes) as pool:
        pool.map(run_blast, my_b_objs)
    print('############Done############')


if __name__ == "__main__":
    sys.exit(main(sys.argv))
