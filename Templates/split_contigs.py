#!usr/bin/env python3
"""
This script splits a fasta file with contigs into multiple fasta files that contain
about 1000 contigs per file.

NOTE:
    change 'file =' to the file you need
    change 'tot_head_count ==' to the total of contigs
"""

__author__ = 'Jelle B'
__date__ = 14-1-2020
__version__ = '2020.v1'

file = '/students/2019-2020/Thema06/project-data/bio_corrosie/biocor_bioclear/' \
       'Galaxy19-[SPAdes_on_data_1_and_data_2__contigs_(fasta)].fasta'
o_file = open(file)

head_count = 0
tot_head_count = 0
file_count = 0
data = ''

# for loop that loops over all the lines in the file
for line in o_file:

    # counts the number of headers
    if line.startswith(">"):
        head_count += 1
        tot_head_count += 1

    # writes 1000 contigs to an output file
    if line.startswith(">") and head_count == 1000:
        file_count += 1
        outfile = open("contig_file_{}.fasta".format(0+file_count), 'w')
        outfile.write(data.rstrip())
        data = ''
        head_count = 0
        outfile.close()

    # writes the last contigs to an output file
    elif tot_head_count == 24436:
        file_count += 1
        outfile = open("contig_file_{}.fasta".format(0+file_count), 'w')
        outfile.write(data.strip())
        data = ''
        head_count = 0
        outfile.close()

    data += line

o_file.close()
