#!/usr/bin/env  python3

"""
dit script geeft een raport terug waar de taxa die in
beide blasts voor komen worden weergegeven.

"""
import sys
import argparse

__author__ = 'Bengels'


class SearchFind:
    def __init__(self, file1, file2):
        self.l2 = []
        self.l1 = []
        self.f1 = file1
        self.f2 = file2
        self.items = [self.f1, self.f2]


    def count(self):
        """
        deze module telt de regels van het bestand.
        """
        count = -3
        with open(self.f1) as f:
            for l in f:
                count += 1
        return int(count)

    def search(self):
        """
        deze module zoekt naar de taxa van beide bestanden.

        """
        for file in self.items:
            with open(file) as f:
                for l in f:

                    li = l.split("\t")
                    keys = li[0].strip()
                    if len(keys) < 20 and keys is not '':
                        if not len(self.l1) == SearchFind.count(self):
                            self.l1.append(keys)
                        else:
                            self.l2.append(keys)

    def compare(self):
        """
        deze module vergelijkt de taxa van beide bestanden met elkaar.
        """
        l_dupli = []
        for i in self.l1:
            if i in self.l2:
                l_dupli.append(i)
        return l_dupli

    def write_file(self):
        """
        deze module schrijft een bestand weg waar alle taxa die in biede bestanden voor komen in staan.
        """
        new_file = open('similarity_file.txt', 'a')
        new_file.write("file1: {} \nfile2: {} \n{} \n\n".format(self.f1, self.f2, SearchFind.compare(self)))
        new_file.close()



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file1", help="file1")
    parser.add_argument('file2', help='file2')
    args = parser.parse_args()
    f1 = args.file1
    f2 = args.file2

    pro = SearchFind(f1, f2)
    pro.search()
    pro.count()
    pro.write_file()

main()
