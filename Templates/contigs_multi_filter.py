#!/usr/bin/env/ python3
"""
This program makes it possible to run process_blast_output.py with multiple files.
This program takes the arguments txt files, length, identity and a output file
"""

__author__ = 'Bart Engels'
__data__ = '14-01-2020'
__version__ = "v1_2020"

import sys
import os
import glob
import argparse
from multiprocessing import Pool


class PythonObj:
    """
    This module makes it easier to use the process_blast_output.py with multiple files
    by giving the module the arguments file length and id.
    """

    def __init__(self, file, length, identity):
        self.file = file
        self.length = length
        self.identity = identity
        self.out = file[0:-4] + "_filter.txt"
        self.statement = "python3 process_blast_output.py -f {} -l {} -i {} -o {}" \
            .format(self.file, self.length, self.identity, self.out)

    def run(self):
        """
        Is used in the pool function in the main()
        """
        statement_line = self.statement
        os.system(statement_line)


def run_blast(b_obj):
    """
    This methode runs the stament
    that is hardcoded in the init

    """
    b_obj.run()


def main(args):
    """
    this makes sure te program can be used.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "-patway", help="patway to directory with files.")
    parser.add_argument("-l", "-length", help="")
    parser.add_argument("-i", "-identity", help="")
    args = parser.parse_args()
    pathway = args.p
    length = args.l
    identity = args.i
    list_fasta_file = glob.glob(str(pathway) + "*.txt")
    my_b_objs = list()
    for file in list_fasta_file:
        my_b_objs.append(PythonObj(file, length, identity))
    nb_processes = len(my_b_objs)

    with Pool(processes=nb_processes) as pool:
        pool.map(run_blast, my_b_objs)
    print('############Done############')


if __name__ == "__main__":
    sys.exit(main(sys.argv))
